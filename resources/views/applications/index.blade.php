@extends('layouts.base')
@section('content')
    @include('notifications.alerts')

    <h2>{{auth_user()->name}}</h2>

    <h3>All applications</h3>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Book denomination</th>
            <th scope="col">Return date</th>
            <th scope="col">Status</th>
        </tr>
        </thead>
        <tbody>
        @foreach($applications as $application)
            @if(auth_user()->id == $application->user_id)
                <tr>
                    <td>{{$application->book->denomination}}</td>
                    <td>{{date($application->return_date)}}</td>

                    @if($application->book->status == 'returned')
                        <td>{{$application->book->status}}</td>
                    @else
                        @if($application->return_date <= Carbon\Carbon::now())
                            <td>{{$application->book->status}}</td>
                        @else
                            <td>expectedлоллорор</td>
                        @endif
                </tr>
            @endif
            @endif
        @endforeach
        </tbody>
    </table>
@endsection
