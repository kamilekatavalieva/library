@extends('layouts.base')
@section('content')

    <h3>Application form</h3>
    <form enctype="multipart/form-data" method="post" action="{{route('books.applications.store', ['book'=> $book])}}">
        @csrf
        <div class="form-group">
            <label for="library_cart">Library cart</label>
            <input type="text" class="form-control" id="library_cart" name="library_cart" value="{{$library_cart}}"
                   disabled>
        </div>
        <div class="form-group">
            <input type="hidden" class="form-control" id="book_id" name="book_id">
        </div>
        <div class="form-group">
            <label for="author">Book author</label>
            <input type="text" class="form-control" id="author" name="author" value="{{$book->author}}" disabled>
        </div>
        <div class="form-group">
            <label for="denomination">Book denomination</label>
            <input type="text" class="form-control" id="denomination" name="denomination"
                   value="{{$book->denomination}}" disabled>
        </div>
        <div class="form-group">
            <label for="return_date">Return date</label>
            <input type="text" class="form-control border-success  @error('return_date') is-invalid border-danger
                        @enderror" id="return_date" name="return_date">
        </div>
        @error('return_date')
        <p class="text-danger">{{ $message }}</p>
        @enderror
        <button type="submit" class="btn btn-outline-danger">Apply</button>
    </form>
@endsection
