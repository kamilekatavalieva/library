<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Library</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
{{--            {{ config('app.name', 'Library') }}--}}
            Library
        </a>

        @if(!empty(auth_user()))
            <a class="navbar-brand" href="{{route('books.applications.index', compact('book'))}}">My book</a>
        @endif
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ ('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">


            <ul class="navbar-nav ml-auto">
                @if(session()->exists('user_id'))
                    <li class="nav-item">
                        <a class="nav-link" href="">{{auth_user()->name}}</a>
                    </li>
                    <li class="nav-item">

                        <form action="{{route('sessions.delete')}}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-link" style="color: #4a5568">Logout</button>
                        </form>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('sessions.login')}}">{{ ('Login') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('users.register')}}">{{ __('Register') }}</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<div class="flex-center position-ref full-height">
    <div class="content container-sm">
        @yield('content')
    </div>
</div>
</body>
</html>
