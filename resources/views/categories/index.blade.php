@extends('layouts.base')

@section('content')
    @include('notifications.alerts')
    @if(!empty(auth_user()))
        <div class="mt-3">
            <h1>{{auth_user()->name}} Your library card number {{auth_user()->library_cart}}</h1>
        </div>
    @endif
    <div style="padding-bottom: 30px;">
        <div class="row">
            <div class="col-md-3 category-list">
                <h1 style="font-family: 'Dancing Script', cursive;">All categories</h1>
                <div class="d-flex flex-column">
                    @foreach($categories as $category)
                        <span>{{$category->title}}</span>
                    @endforeach
                </div>
            </div>

            <div class="row book--list col-sm-9">
                @foreach($categories as $category)
                    <div class="col-sm-12">
                        <h3 class="">{{$category->title}}</h3>
                        <div class="inner d-flex w-100">
                            <div class="row">
                                @foreach($books as $book)

                                    @if($category->id == $book->category_id)
                                        <div class="col-md-4  my-2">
                                            <div class="card h-100">
                                                <img src="{{asset('/storage/' . $book->picture)}}" class="card-img-top"
                                                     alt="{{asset('/storage/' . $book->picture)}}">
                                                <div class="card-body">
                                                    <h5 class="card-title">{{$book->denomination}}</h5>
                                                    <p class="card-text"><b>Author:</b> {{$book->author}}</p>
                                                    @if($book->status == 'returned')
                                                        <div class="col-md-12">
                                                            <div class="card-body">
                                                                <a class="btn btn-primary"
                                                                   href={{route('books.applications.create', ['book' => $book])}}>Receive</a>
                                                            </div>
                                                        </div>
                                                    @elseif($book->status == 'expected')
                                                        <div class="col-md-12">
                                                            <div class="card-body">
                                                                <p>Expected</p>
                                                                <p>{{date($book->return_date)}}</p>
                                                            </div>
                                                        </div>

                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </div>
    <div class="row justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $categories->links('pagination::bootstrap-4') }}
        </div>
    </div>
@endsection

