<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class BookFactory extends Factory
{
    /**
     * @return array
     */
    public function definition():array
    {
        return [
            'denomination' => $this->faker->jobTitle(),
            'picture' => $this->getImage(rand(1, 40)),
            'author' => $this->faker->name(),
            'status' => $this->faker->randomElement($arr = ['returned', 'expected']),
            'return_date' => $this->faker->date(),
            'category_id' => rand(1, 5)
        ];
    }

    /**
     * @param int $image_number
     * @return string
     */
    private function getImage($image_number = 1): string
    {
        $path = storage_path() . "/book_pictures/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('pictures/'.$image_name, $resize->__toString());
        return 'pictures/'.$image_name;

    }
}
