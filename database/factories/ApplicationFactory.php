<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ApplicationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition():array
    {
        return [
            'return_date' => $this->faker->date(),
            'user_id' => rand(1, 10),
            'book_id' => rand(1, 40)
        ];
    }
}
