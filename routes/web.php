<?php

use App\Http\Controllers\ApplicationsController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\SessionsController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CategoriesController::class, 'index']);

Route::resource('categories', CategoriesController::class)->only('index');
Route::resource('books.applications', ApplicationsController::class)->only(['index', 'create', 'store']);
Route::get('/users/register', [UsersController::class, 'register'])->name('users.register');
Route::post('/users', [UsersController::class, 'store'])->name('users.store');
Route::get('/login', [SessionsController::class, 'create'])->name('sessions.login');
Route::post('/login', [SessionsController::class, 'store'])->name('sessions.store');
Route::delete('/logout', [SessionsController::class, 'destroy'])->name('sessions.delete');

