<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Application extends Model
{
    use HasFactory;


    /**
     * @var string[]
     */
    protected $fillable = ['return_date', 'user_id', 'book_id'];


    /**
     * @return BelongsTo
     */
    public function book():BelongsTo
    {
        return $this->belongsTo(Book::class);
    }


    /**
     * @return BelongsTo
     */
    public function user():BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
