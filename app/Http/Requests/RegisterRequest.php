<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize():bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules():array
    {
        return [
            'name' => ['bail','required','between:2, 255'],
            'address' => ['bail','required', 'string'],
            'passport_number' => ['bail','required','unique:users,passport', 'min:7'],
            'library_cart' => ['bail', 'required','unique:users,library_card'],
            'password' => ['bail','required','confirmed','min:8'],
        ];
    }
}
