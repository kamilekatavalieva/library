<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;

class HomeController extends Controller
{
    /**
     * @return RedirectResponse
     */
    public function home():RedirectResponse
    {
        if(session()->exists('user_id'))
        {
            return  redirect()->route('categories.index');
        }
        return  redirect()->route('sessions.login');
    }
}
