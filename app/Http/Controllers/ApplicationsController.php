<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApplicationRequest;
use App\Models\Application;
use App\Models\Book;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ApplicationsController extends Controller
{

    /**
     * @param Book $book
     * @return \Illuminate\Contracts\Foundation\Application|Factory|View
     */
    public function index(Book $book)
    {
        $applications = Application::all();
        return view('applications.index', compact('applications', 'book'));
    }


    /**
     * @param Book $book
     * @return \Illuminate\Contracts\Foundation\Application|Factory|View
     */
    public function create(Book $book)
    {
        if(session()->exists('user_id')) {
            $library_cart = auth_user()->library_cart;
            return view('applications.create', compact('book','library_cart'));
        }
        return view('sessions.create');
    }

    /**
     * @param ApplicationRequest $request
     * @param Book $book
     * @return RedirectResponse
     */
    public function store(ApplicationRequest $request, Book $book):RedirectResponse
    {
        $date = $request->input('return_date');

        if($book->status == 'returned'){
            $book->status = 'expected';
        } else {
            $book->status = 'returned';
        }
        $book->return_date = Carbon::parse($date);
        if($book->return_date <= Carbon::now()) {
            return redirect()->back()->with('error', 'Please enter the correct date!');
        }
        $book->save();

        $application= new Application();
        $application->user_id = auth_user()->id;
        $application->book_id = $book->id;
        $application->return_date = Carbon::parse($date);
        $application->save();
        return redirect()->route('categories.index');
    }
}
